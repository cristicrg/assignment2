
const FIRST_NAME = "Robert Cristian";
const LAST_NAME = "Gheorghe";
const GRUPA = "1083";

class Employee {
    constructor(name,surname,salary){
        this.name = name ;
        this.surname = surname;
        this.salary = salary; }
    getDetails(){ return `${this.name} ${this.surname} ${this.salary}`; }}

class SoftwareEngineer extends Employee {
   constructor(name,surname,salary,experience=''){
       super(name,surname,salary);
       if(experience==='')
	   {this.experience='JUNIOR';}
   else
	   {this.experience=experience;}
   }
   applyBonus(){
       switch(this.experience){
           case 'JUNIOR' : return this.salary*1.1;
           case 'MIDDLE' : return this.salary*1.15;
           case 'SENIOR' : return this.salary*1.2;
           default : return this.salary*1.1; }}
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

